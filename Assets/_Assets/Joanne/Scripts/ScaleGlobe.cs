using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleGlobe : MonoBehaviour
{
    public Transform objectToScale;
    public float minScale = 0.5f;
    public float maxScale = 2.0f;
    public float scaleSpeed = 0.1f;

    private void Update()
    {
        // Get joystick input from Oculus Touch controller
        float joystickInput = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch).y;

        // Calculate new scale factor
        float scaleFactor = Mathf.Clamp(objectToScale.localScale.x + joystickInput * scaleSpeed, minScale, maxScale);

        // Apply the new scale to the object
        objectToScale.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
    }
}
