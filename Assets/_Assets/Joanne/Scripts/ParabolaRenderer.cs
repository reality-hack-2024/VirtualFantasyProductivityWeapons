using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ParabolaRenderer : MonoBehaviour
{
    public Transform pointA;
    public Transform pointB;
    public Transform sphereCenter;
    public GameObject lineRendererPrefab;
    public GameObject parentObject;
    public int segmentCount = 20;
    public float curveHeight = 5.0f; // Height of the curve above the sphere

    public Transform[] pointTs;
    public LineRenderer lRenderer;

    void Start()
    {
        /*
        if (lineRendererPrefab == null || pointA == null || pointB == null || sphereCenter == null)
        {
            Debug.LogError("Required components are not assigned!");
            return;
        };
        */

        //lRenderer = lineRendererObject.GetComponent<LineRenderer>();
        //BuildAirplanePath();
    }

    void Update()
    {
        //DrawAirplanePath();
        OnlyDrawAirplanePath();
    }


    public void SetPoints(Transform pointA, Transform pointB, Transform sphereCenter)
    {
        this.pointA = pointA;
        this.pointB = pointB;
        this.sphereCenter = sphereCenter;

        if (lineRendererPrefab == null || pointA == null || pointB == null || sphereCenter == null)
        {
            Debug.LogError("Required components are not assigned!");
            return;
        };

        //DrawAirplanePath();
        BuildAirplanePath();
    }

    /*
    void DrawAirplanePath()
    {
        Vector3 relativeStart = pointA.position - sphereCenter.position;
        Vector3 relativeEnd = pointB.position - sphereCenter.position;

        float radius = relativeStart.magnitude; // Assuming the sphere is uniform and pointA is on the surface
        GameObject lineRendererCarrier = Instantiate(lineRendererObject, sphereCenter.position, Quaternion.identity);
        lineRendererCarrier.transform.SetParent(parentObject.transform);
        lineRendererObject.GetComponent<LineRenderer>().positionCount = segmentCount;

        for (int i = 0; i < segmentCount; i++)
        {
            float t = i / (segmentCount - 1.0f);
            Vector3 basePoint = Vector3.Slerp(relativeStart, relativeEnd, t).normalized * radius;

            // Adjust height to create an arch that starts and ends at zero height
            float heightFactor = Mathf.Sin(t * Mathf.PI) * curveHeight;
            Vector3 heightAddedPoint = basePoint + heightFactor * sphereCenter.up;
            lRenderer.SetPosition(i, heightAddedPoint + sphereCenter.position);
        };
    }*/

    void BuildAirplanePath()
    {
        Vector3 relativeStart = pointA.position - sphereCenter.position;
        Vector3 relativeEnd = pointB.position - sphereCenter.position;

        float radius = relativeStart.magnitude; // Assuming the sphere is uniform and pointA is on the surface
        GameObject lineRendererCarrier = Instantiate(lineRendererPrefab, sphereCenter.position, Quaternion.identity);
        lineRendererCarrier.transform.SetParent(parentObject.transform);


        // line renderer uses global Vector3s for its drawing.
        // so we need to build our arch with transforms so they can hold our curve
        // then we can use the pos of those points to redraw the curve each frame.

        pointTs = new Transform[segmentCount];


        lRenderer = lineRendererCarrier.GetComponent<LineRenderer>();
        lRenderer.positionCount = segmentCount;

        for (int i = 0; i < segmentCount; i++)
        {
            float t = i / (segmentCount - 1.0f);
            Vector3 basePoint = Vector3.Slerp(relativeStart, relativeEnd, t).normalized * radius;

            // Adjust height to create an arch that starts and ends at zero height
            float heightFactor = Mathf.Sin(t * Mathf.PI) * curveHeight;

            var avgPoint = (pointA.position + pointB.position) * 0.5f;
            var avgDir = (avgPoint - sphereCenter.position).normalized;
            Vector3 heightAddedPoint = basePoint + heightFactor * avgDir;// sphereCenter.up;

            pointTs[i] = new GameObject($"Curve Point {i}").transform;
            pointTs[i].parent = parentObject.transform;
            pointTs[i].position = heightAddedPoint + sphereCenter.position;
            //lRenderer.SetPosition(i, heightAddedPoint + sphereCenter.position);
        };


    }

    void OnlyDrawAirplanePath()
    {
        if (pointTs == null)
            return;

        for (int i = 0; i < pointTs.Length; i++)
        {
           lRenderer.SetPosition(i, pointTs[i].position);
        };
    }
}