using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabShowGlobe : MonoBehaviour
{
    public GameObject objectToShow;
    public OVRInput.Button grabButton = OVRInput.Button.SecondaryHandTrigger;

    private bool isObjectVisible = false;

    void Start()
    {
        // Initially hide the object
        objectToShow.SetActive(false);
    }

    void Update()
    {
        // Check if the grab button is pressed on the left Oculus Touch controller
        if (OVRInput.GetDown(grabButton, OVRInput.Controller.LTouch))
        {
            // Toggle visibility of the object
            isObjectVisible = !isObjectVisible;
            objectToShow.SetActive(isObjectVisible);
        }
    }
}
