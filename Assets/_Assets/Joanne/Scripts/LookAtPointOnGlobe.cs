using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPointOnGlobe : MonoBehaviour
{
    //x is lat y is long
    public List<Vector2> pois;

   // float latitude = ; // San Francisco
   // float longitude = -122.4194f; // San Francisco

    public Transform globeTransform; // Reference to the sphere's transform
    public GameObject pointPref;
    public float globeRadius = 1f; // Radius of the sphere

    public void Start()
    {

        pois.Add(new Vector2(40.753163654f, -73.984496062f)); //nyc
        pois.Add(new Vector2(42.360001f, -71.092003f));//boston
        pois.Add(new Vector2(37.7749f, -122.4194f));//san fran

        foreach (var p in pois)
        {
            GeneratePoi(LatLongToSpherical(p.x, p.y));
        }
    }

    // Function to convert latitude and longitude to spherical coordinates
    Vector3 LatLongToSpherical(float latitude, float longitude)
    {
        // Convert latitude and longitude to radians
        float latRad = Mathf.Deg2Rad * latitude;
        float longRad = Mathf.Deg2Rad * longitude;

        // Calculate spherical coordinates
        float x = globeRadius * Mathf.Sin(latRad) * Mathf.Cos(longRad);
        float y = globeRadius * Mathf.Cos(latRad);
        float z = globeRadius * Mathf.Sin(latRad) * Mathf.Sin(longRad);

        return new Vector3(x, y, z);
    }

    public GameObject GeneratePoi(Vector3 pos)
    {
        return Instantiate(pointPref, pos, Quaternion.identity);
    }
}
