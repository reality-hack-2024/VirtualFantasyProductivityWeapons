using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class LatLongToSphere : MonoBehaviour
{

    public GameObject pointPref;

    public bool testPostionOnStart;
    public Transform globe;
    public float globeRadius = 1f; // Radius of the sphere

    public string jsonFilePath;

    public Transform cameraTransform;

    public void Start()
    {

        // UpdatePin();
    }

    public void Update()
    {
        if (testPostionOnStart)
        {
            testPostionOnStart = false;
            AddArc(new Location
            {
                locationName = "Origin",
                coordinates = new Coordinates
                {
                    lat = 0.0f,
                    lng = 0.0f
                }
            },
            new Location
            {
                locationName = "Destination",
                coordinates = new Coordinates
                {
                    lat = 45.0f,
                    lng = -30.0f
                }
            });
        }
    }


    private Vector3 LatLngToXYZ(float latitude, float longitude, float radius)
    {
        Vector3 point = new Vector3(radius, 0, 0);

        point = Quaternion.Euler(0, -longitude, latitude) * point;

        return point;
    }

    public Vector3 GetLocationGlobalPosition(Location location)
    {
        var xyzLocal = LatLngToXYZ(location.coordinates.lat, location.coordinates.lng, globeRadius);
        var xyzGlobal = globe.TransformPoint(xyzLocal);
        return xyzGlobal;
    }

    public void AddPin(Location location)
    {
        var locationXYZ = GetLocationGlobalPosition(location);

        GameObject obj = Instantiate(pointPref, locationXYZ, Quaternion.identity, globe);

        // Set object's name
        obj.name = location.locationName;

        Debug.Log("Instantiated " + obj.name);
    }

    public void AddArc(Location origin, Location destination)
    {
        var originXYZ = GetLocationGlobalPosition(origin);
        var originTrans = Instantiate(pointPref, originXYZ, Quaternion.identity, globe);

        var destinationXYZ = GetLocationGlobalPosition(destination);
        var destinationTrans = Instantiate(pointPref, destinationXYZ, Quaternion.identity, globe);

        DrawParabola(originTrans.transform, destinationTrans.transform);
    }

    // Point on the sphere that should face the camera
    public void SpinGlobeToCamera(Vector3 selectedPoint)
    {
        // Calculate the direction from the sphere's center to the selected point
        Vector3 directionToSelectedPoint = selectedPoint - globe.position;

        // Calculate the rotation angle around the Y-axis needed to align the selected point with the camera
        float angleToRotate = Vector3.SignedAngle(directionToSelectedPoint, cameraTransform.forward, Vector3.up);

        // Apply the rotation to the sphere around the Y-axis
        transform.Rotate(Vector3.up, angleToRotate);
    }

    public GameObject ParabolaRendererManager;
    public GameObject ParabolaRendererPrefab;

    public void DrawParabola(Transform start, Transform end)
    {
        // Instantiate prefab
        GameObject obj = Instantiate(ParabolaRendererPrefab, globe.position, Quaternion.identity);

        // Set object's parent
        obj.transform.SetParent(ParabolaRendererManager.transform);

        //Set Parabola
        obj.GetComponent<ParabolaRenderer>().SetPoints(start, end, globe);

        Debug.Log("Instantiated parabola" + start + end);

    }
}

[System.Serializable]
public class Coordinates
{
    public float lng;
    public float lat;
}

[System.Serializable]
public class Location
{
    public string locationName;
    public Coordinates coordinates;
}