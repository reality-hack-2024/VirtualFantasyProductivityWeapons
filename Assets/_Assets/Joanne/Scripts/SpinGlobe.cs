using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinGlobe : MonoBehaviour
{
    public Transform objectToRotate;
    public float rotationSpeed = 50f;

    void Update()
    {
        // Get joystick input from left Oculus Touch controller
        float joystickInputX = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch).x;

        // Calculate rotation amount based on joystick input
        float rotationAmount = joystickInputX * rotationSpeed * Time.deltaTime;

        // Rotate the object around its Y-axis
        objectToRotate.Rotate(Vector3.up, rotationAmount);
    }
}
