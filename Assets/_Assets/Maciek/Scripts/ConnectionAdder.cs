using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class ConnectionAdder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <param name="values"></param>
    public void AddConnection(string[] values)
    {
        var origin = values[0];
        var destination = values[1];

        GetComponent<TMP_Text>().text = $"Connection: {origin} to {destination}";
    }

    public void AddLocationToMap(string[] values)
    {
        var location = values[0];

        GetComponent<TMP_Text>().text = $"Added location: {location}";
    }

    public void AddStay(string[] values)
    {
        var stayLocation = values[0];

        GetComponent<TMP_Text>().text = $"Added stay: {stayLocation}";
    }
}
