using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class VoiceIntentHandler : MonoBehaviour
{
    public LatLongToSphere LatLongToSphere;
    //
   
    public StringEvent TileEvent;
    //public UnityEvent<string> TileEvent;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddLocationToMap(string[] values)
    {
        if (values[0] == "" || values[1] == "" || values[2] == "") return;

        var name = values[0];
        var lat = float.Parse(values[1]);
        var lng = float.Parse(values[2]);

        Debug.Log($"Adding lat:{lat}, lon: {lng}");

        var coords = new Coordinates { lng = lng, lat = lat };
        var location = new Location
        {
            locationName = name,
            coordinates = coords
        };

        LatLongToSphere.AddPin(location);
    }

    public void AddArcToMap(string[] values)
    {

        if (values[1] == "" || values[2] == "" || values[4] == "" || values[5] == "") return;

        var originName = values[0];
        var originLat = float.Parse(values[1]);
        var originLng = float.Parse(values[2]);

        var destinationName = values[3];
        var destinationLat = float.Parse(values[4]);
        var destinationLng = float.Parse(values[5]);

        var originLocation = new Location
        {
            locationName = originName,
            coordinates = new Coordinates
            {
                lng = originLng,
                lat = originLat
            }
        };

        var destinationLocation = new Location
        {
            locationName = destinationName,
            coordinates = new Coordinates
            {
                lng = destinationLng,
                lat = destinationLat
            }
        };

        LatLongToSphere.AddArc(originLocation, destinationLocation);

        TileEvent.Invoke($"{originLocation.locationName} to {destinationLocation.locationName}");
    }

    public void AddStay(string[] values)
    {
        var locationName = values[0];

        TileEvent.Invoke($"{locationName}");
    }
}

//allows us to pass string with unity events
[System.Serializable]
public class StringEvent : UnityEvent<string>
{

}
