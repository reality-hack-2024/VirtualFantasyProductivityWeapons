using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class EventOnTrigger : MonoBehaviour
{
    //sword and just hands was also triggering this way only this collider will trigger our event
    public Collider targetCollider;

    public UnityEvent TriggerEvent;

    private bool _waited = false;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(InitialWait());
    }

    void OnTriggerEnter(Collider collider)
    {
        if(targetCollider != null && targetCollider != collider) 
        {
            return;
        }

        if (_waited)
        {
            TriggerEvent.Invoke();
        }

    }

    IEnumerator InitialWait()
    {
        yield return new WaitForSeconds(3);
        _waited = true;
    }
}
