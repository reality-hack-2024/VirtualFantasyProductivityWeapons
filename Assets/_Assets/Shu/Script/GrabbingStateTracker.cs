using Oculus.Interaction.HandGrab;
using UnityEngine;

public class GrabbingStateTracker : MonoBehaviour
{
    public Tile tile;
    public HandGrabInteractor handGrabInteractor; // Reference to the HandGrabInteractor
    private bool wasGrabbing = false;

    void Update()
    {
        if(handGrabInteractor == null)
        {
            wasGrabbing = false;
            tile.isGrabbed = false;
                return;
        }
        bool isCurrentlyGrabbing = handGrabInteractor.IsGrabbing; // Check the current grabbing state

        // Check if the grabbing state has changed from not grabbing to grabbing
        if (!wasGrabbing && isCurrentlyGrabbing)
        {
            Debug.Log("Object has been grabbed.");

        }
        
        // Check if the grabbing state has changed from grabbing to not grabbing
        if (wasGrabbing && !isCurrentlyGrabbing)
        {
            Debug.Log("Object has been released.");
        }

        // Update the previous grabbing state
        wasGrabbing = isCurrentlyGrabbing;
        tile.isGrabbed = isCurrentlyGrabbing;
    }
}