using System.Collections;
using System.Collections.Generic;
using Oculus.Interaction.HandGrab;
using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    public GameObject tilePrefab;
    public Transform spawnPointA;
    public Transform spawnPointB;
    //public HandGrabInteractor handGrabInteractor;
    public HandGrabInteractor[] handGrabInteractors;
    public Collider[] handPinchColliders;

    public Tile instantedTile;

    public float moveSpeed = 5.0f;

//#if UNITY_EDITOR
    public bool editorSpawn;

    void Update()
    {
        if (editorSpawn)
        {
            editorSpawn = false;
            Spawn("STAY MOM");
        }
    }
//#endif

    public void Spawn(string txt)
    {
        //var go = Instantiate(tilePrefab, spawnPointA.position, spawnPointA.rotation, spawnPointA);
        // instantedTile = go.GetComponent<Tile>();
        instantedTile = SpawnTile(txt);
        //instantedTile.grabTracker.handGrabInteractor = handGrabInteractor;
        //instantedTile.multiTracker.handGrabInteractors = handGrabInteractors;

        //instantedTile.Init(this);

        //instantedTile.text.text = txt;
        StartCoroutine(MoveTile(instantedTile.transform));
    }

    public Tile SpawnTile(string txt)
    {
        var go = Instantiate(tilePrefab, spawnPointA.position, spawnPointA.rotation, spawnPointA);

        Tile newTile = go.GetComponent<Tile>();
        newTile.Init(this);
        newTile.text.text = txt;

        return newTile;
    }

    IEnumerator MoveTile(Transform tileTransform)
    {        
        while (Vector3.Distance(tileTransform.position, spawnPointB.position) > 0.0001f)
        {
            if(instantedTile == null || instantedTile.isGrabbed)
            {
                yield break;
            }

            tileTransform.position = Vector3.MoveTowards(tileTransform.position, spawnPointB.position, moveSpeed * Time.deltaTime);
            yield return null;
        }

    }




}
