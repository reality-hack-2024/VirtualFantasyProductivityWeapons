using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTileOnTrigger : MonoBehaviour
{

    private void OnTriggerStay(Collider other)
    {
        var t = other.transform.GetComponentInParent<Tile>();

        if (t != null && !t.isGrabbed)
        {
            t.gameObject.SetActive(false);
        }

    }
}
