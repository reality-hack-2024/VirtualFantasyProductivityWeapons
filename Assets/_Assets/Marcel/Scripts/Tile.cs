using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Oculus.Interaction.HandGrab;

public class Tile : MonoBehaviour
{
    public bool isGrabbed = false;
    public TextMeshPro text;
    //public GrabbingStateTracker grabTracker;
    public MultiGrabStateTracker multiTracker;

    public void Init(TileSpawner spawner)
    {
        multiTracker.Init(spawner);
        multiTracker.GrabEvent.AddListener(ProcessGrabEvent);
    }

    private void OnDestroy()
    {
        multiTracker.GrabEvent.RemoveListener(ProcessGrabEvent);
    }

    public void ProcessGrabEvent(bool setGrabbed)
    {
        isGrabbed = setGrabbed;
    }
}
