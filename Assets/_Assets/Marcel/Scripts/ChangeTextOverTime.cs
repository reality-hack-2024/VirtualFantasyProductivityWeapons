using System.Collections;
using UnityEngine;
using TMPro; // Make sure to include this to work with TextMeshPro

public class ChangeTextOverTime : MonoBehaviour
{
    public TextMeshProUGUI textMeshPro; // Assign your TextMeshPro object in the Inspector
    public float changeInterval = 5f; // Time in seconds between text changes

    public string[] texts;// = new string[] {
      //  "Listening </br> ...",
      //  "Listening"

    //};
    private int currentIndex = 0;
    private Coroutine changeTextCoroutine;

    void OnEnable()
    {
        if (texts == null || texts.Length == 0) return;
        // Restart changing text at intervals when GameObject is enabled
        changeTextCoroutine = StartCoroutine(ChangeTextAtIntervals());
    }

    void OnDisable()
    {
        // Stop the coroutine when the GameObject is disabled to avoid errors
        if (changeTextCoroutine != null)
        {
            StopCoroutine(changeTextCoroutine);
        }
    }

    IEnumerator ChangeTextAtIntervals()
    {
        while (true)
        {
            // Change the text
            textMeshPro.text = texts[currentIndex];

            // Move to the next text, wrapping around if necessary
            currentIndex = (currentIndex + 1) % texts.Length;

            // Wait for the specified interval
            yield return new WaitForSeconds(changeInterval);
        }
    }
}
