using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Oculus.Interaction.GrabAPI;

public class ShieldPositioner : MonoBehaviour
{
    public Transform globeParent;
    public Quaternion initRot = Quaternion.Euler(new Vector3(0, -90, 0));

    public Vector3 smallPos = Vector3.right * -0.04f;
    public Vector3 smallScale = Vector3.one * 0.3f;

    public Vector3 bigPos = Vector3.right * 0.1f;
    public Vector3 bigScale = Vector3.one;

    public HandGrabAPI handGrabAPI;
    public GrabbingRule rule;

    private bool isGrabbing = false;
    private float transitionSpeed = 1f; // Speed of the transition
    private float transitionProgress = 0f; // Progress of the current transition

    // Start is called before the first frame update
    void Start()
    {
        transform.parent = globeParent;
        transform.localPosition = smallPos;
        transform.localRotation = initRot;
        transform.localScale = smallScale;
    }

    void Update()
    {

        bool currentGrabbingState = handGrabAPI.IsHandPalmGrabbing(rule);
        if (currentGrabbingState != isGrabbing)
        {
            isGrabbing = currentGrabbingState;
            transitionProgress = 0f; // Reset progress whenever the grabbing state changes
        }

        if (transitionProgress >= 1f)
            return;

        Vector3 targetPos = isGrabbing ? bigPos : smallPos;
        Vector3 targetScale = isGrabbing ? bigScale : smallScale;

        // Increment transition progress
        transitionProgress += Time.deltaTime * transitionSpeed;
        // Clamp transitionProgress to ensure it does not exceed 1
        transitionProgress = Mathf.Min(transitionProgress, 1f);

        // Interpolate based on the current transition progress
        float smoothStep = Mathf.SmoothStep(0f, 1f, transitionProgress);
        transform.localPosition = Vector3.Lerp(transform.localPosition, targetPos, smoothStep);
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, smoothStep);
    }

}
