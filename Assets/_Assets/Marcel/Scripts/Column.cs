using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour
{
    public List<Tile> tiles;
    public Transform ghostTileTrans;
    public Transform origin;

    public float totalLength;

    public Tile candidateTile;

    public float roundedDistance = 0.1f;
    public ColumnManager cMan;
    void Start()
    {
        
        if(tiles == null )
        {
            tiles = new();
        }
        GhostHide();
    }

    void Update()
    {
        if (candidateTile == null)
            return;

        if (candidateTile.isGrabbed)
        {
            //indicate where the current hold will position with ghost
            GhostFollow();
        }
        else
        {
            PlaceCandidate();
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        var newTile = col.attachedRigidbody.gameObject.GetComponent<Tile>();
        if (newTile == null)
            return;

        candidateTile = newTile;
        //cMan.currentColumn = this;
        cMan.ChangeCurrentColumn(this);
    }

    private void OnTriggerExit(Collider col)
    {
        var newTile = col.attachedRigidbody.gameObject.GetComponent<Tile>();
        if (newTile == null)
            return;

        if(candidateTile == newTile || candidateTile == null)
        {
            GhostHide();
        }

        
        if (!TryRemoveTile(newTile))
        {
            Debug.Log("Could Not Remove Tile");
        }
    }

    bool TryAddNewTile()
    {

        if (tiles.Contains(candidateTile))
        {
            return false;
        }

        tiles.Add(candidateTile);
        return true;
    }

    void DenyAddTile()
    {
        //deny logic
    }

    bool TryRemoveTile(Tile tile)
    {
        if (tiles.Contains(tile))
        {
            tiles.Remove(tile);
            return true;
        }

        return false;
    }

    public void PlaceCandidate()
    {
        if (TryAddNewTile())
        {
            GhostHide();
            TransformFollowTarget(candidateTile.transform, candidateTile.transform);
        }
        else
        {
            DenyAddTile();
        }
    }

    public void GhostFollow()
    {
        TransformFollowTarget(ghostTileTrans, candidateTile.transform);
        //ghostTileTrans.position = candidateTrans.position;
        //float xPos = ghostTileTrans.localPosition.x;
        //ghostTileTrans.localPosition = new Vector3(xPos, 0, 0);
        ghostTileTrans.gameObject.SetActive(true);
    }
    public void TransformFollowTarget(Transform followerTrans, Transform targetTrans)
    {
        followerTrans.parent = origin;
        followerTrans.position = targetTrans.position;
        // Snap the z-position to the nearest interval
        var zPos = Mathf.Round(followerTrans.localPosition.z / roundedDistance) * roundedDistance;
        followerTrans.localPosition = new Vector3(0, 0, zPos);
        followerTrans.localRotation = Quaternion.identity;
    }

    public void GhostHide()
    {
        ghostTileTrans.gameObject.SetActive(false);
    }


    /*
    //NOT SURE IF THIS WORKS
    float CalcTime(Transform tileTran)
    {
        //return Mathf.InverseLerp(0, totalLength, tileTran.localPosition.x);
    }*/
}
