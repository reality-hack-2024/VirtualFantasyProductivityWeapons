using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordTileManager : MonoBehaviour
{
    public int numberOfLines = 6;
    public float distanceBetweenLines = 0.05f;
    public float timeLineLength = 0.5f;
    public float snapZDistance = 0.1f;
    public float upOffset = 0.005f;
    public TimeLine[] timeLines;

    public Transform spawnOriginTrans;

    // public List<Tile> tiles;
    public Transform ghostTileTrans;
    public Transform origin;

    //public float totalLength;

    public Tile candidateTile;

    
    //public ColumnManager cMan;
    void Start()
    {

        if (timeLines == null)
        {
            timeLines = new TimeLine[numberOfLines];
        }

        /*
        for (int i = 0; i < numberOfLines; i++)
        {
            var go = new GameObject("timeLinePoint");
            go.transform.parent = spawnOriginTrans;
            //go.transform.position = spawnOriginTrans.position;
            go.transform.rotation = Quaternion.identity;
            //Instantiate(columnPrefab, spawnOriginTrans.position, spawnOriginTrans.rotation, spawnOriginTrans);
            go.transform.localPosition = Vector3.right * distanceBetweenLines * i;
            //var c = go.GetComponentInChildren<Column>();
            //columns[i] = c;
            //c.cMan = this;
            timeLines[i] = new();
            timeLines[i].tiles = new();
        }
        
        if (tiles == null)
        {
            tiles = new();
        }*/
        GhostHide();
    }

    void Update()
    {
        if (candidateTile == null)
            return;

        if (candidateTile.isGrabbed)
        {
            //indicate where the current hold will position with ghost
            GhostFollow();
            Debug.Log("Following?");
        }
        else
        {
            PlaceCandidate();
            Debug.Log("Placceing?");

        }
    }

    private void OnTriggerEnter(Collider col)
    {
        var newTile = col.attachedRigidbody?.gameObject?.GetComponent<Tile>();
        if (newTile == null)
            return;

        candidateTile = newTile;
        //cMan.currentColumn = this;
       // cMan.ChangeCurrentColumn(this);
       //
    }

    private void OnTriggerExit(Collider col)
    {
        var newTile = col.attachedRigidbody?.gameObject?.GetComponent<Tile>();
        if (newTile == null)
            return;

        if (candidateTile == newTile || candidateTile == null)
        {
            GhostHide();
        }


        if (!TryRemoveTile(newTile))
        {
            Debug.Log("Could Not Remove Tile");
        }
    }

    bool TryAddNewTile()
    {

        /*
        if (tiles.Contains(candidateTile))
        {
            return false;
        }

        tiles.Add(candidateTile);
        */
        return true;

    }

    void DenyAddTile()
    {
        //deny logic
    }

    bool TryRemoveTile(Tile tile)
    {
        /*
        if (tiles.Contains(tile))
        {
            tiles.Remove(tile);
            return true;
        }
        */
        return false;
    }

    public void PlaceCandidate()
    {
        if (TryAddNewTile())
        {
            GhostHide();
            TransformFollowTarget(candidateTile.transform, candidateTile.transform);
            candidateTile = null;
        }
        else
        {
            DenyAddTile();
        }
    }

    public void GhostFollow()
    {
        TransformFollowTarget(ghostTileTrans, candidateTile.transform);
        //ghostTileTrans.position = candidateTrans.position;
        //float xPos = ghostTileTrans.localPosition.x;
        //ghostTileTrans.localPosition = new Vector3(xPos, 0, 0);
        ghostTileTrans.gameObject.SetActive(true);
    }
    public void TransformFollowTarget(Transform followerTrans, Transform targetTrans)
    {
        followerTrans.parent = origin;
        followerTrans.position = targetTrans.position;
        // Snap the z-position to the nearest interval
        var zSnappedPos = Mathf.Round(followerTrans.localPosition.z / snapZDistance) * snapZDistance;
        var xSnappedPos = Mathf.Round(followerTrans.localPosition.x / distanceBetweenLines) * distanceBetweenLines;
        followerTrans.localPosition = new Vector3(xSnappedPos, upOffset, zSnappedPos);
        followerTrans.localRotation = Quaternion.identity;
    }

    public void GhostHide()
    {
        ghostTileTrans.gameObject.SetActive(false);
    }


    public void PositionTileBasedOnTrackAndTime(Transform tileTransform, int track, float time)
    {
        // Assuming origin, snapZDistance, and distanceBetweenLines are defined in your class
        tileTransform.parent = origin; // Set the parent transform to keep things organized

        // Convert track and time to x and z positions
        // Assuming track represents the x-axis position and time represents the z-axis position
        float xPosition = track * distanceBetweenLines; // Calculate x position based on the track
        float zPosition = time * snapZDistance; // Calculate z position based on the time

        // Now, snap these positions to your grid
        var zSnappedPos = Mathf.Round(zPosition / snapZDistance) * snapZDistance;
        var xSnappedPos = Mathf.Round(xPosition / distanceBetweenLines) * distanceBetweenLines;

        // Set the local position relative to the parent (origin) with snapped positions
        tileTransform.localPosition = new Vector3(xSnappedPos, upOffset, zSnappedPos);

        // Reset rotation to ensure tiles are aligned consistently
        tileTransform.localRotation = Quaternion.identity;
    }



    /*
    //NOT SURE IF THIS WORKS
    float CalcTime(Transform tileTran)
    {
        //return Mathf.InverseLerp(0, totalLength, tileTran.localPosition.x);
    }*/

    private void OnDrawGizmos()
    {
        for (int i = 0; i < numberOfLines; i++)
        {
            var xPos = Vector3.right * distanceBetweenLines * i;
            Gizmos.DrawLine(spawnOriginTrans.TransformPoint(xPos), spawnOriginTrans.TransformPoint(xPos + Vector3.forward * timeLineLength));
        }
    }
}

[System.Serializable]
public class TimeLine
{
    public List<Tile> tiles;
}
