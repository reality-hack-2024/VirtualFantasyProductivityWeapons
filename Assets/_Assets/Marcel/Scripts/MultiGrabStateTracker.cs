using Oculus.Interaction.HandGrab;
using UnityEngine;
using UnityEngine.Events;

public class MultiGrabStateTracker : MonoBehaviour
{
    public HandGrabInteractor[] handGrabInteractors;
    public Collider[] handPinchColliders;
    private bool wasGrabbing = false;
    public BoolEvent GrabEvent;

    private bool[] isColliderColliding;

    public void Init(TileSpawner spawner)
    {
        handGrabInteractors = spawner.handGrabInteractors;
        handPinchColliders = spawner.handPinchColliders;

        if(handGrabInteractors.Length != handPinchColliders.Length)
        {
            Debug.LogError("should have set a pinch collider for each interactor this will break");
        }

        isColliderColliding = new bool[handPinchColliders.Length];
    }

    void Update()
    {
        if (handGrabInteractors == null || handGrabInteractors.Length == 0)
        {
            // If no interactors are assigned or the array is empty, set everything to not grabbing
            if (wasGrabbing)
            {
                GrabEvent.Invoke(false);
                wasGrabbing = false;
            }
            return;
        }

        // Check the current grabbing state of any interactor
        bool isCurrentlyGrabbing = false;
        for (int i = 0; i < handGrabInteractors.Length; i++)
        {

            if (handGrabInteractors[i] != null && handGrabInteractors[i].IsGrabbing && isColliderColliding[i])
            {
                isCurrentlyGrabbing = true;
                break; // Exit the loop early if any interactor is grabbing
            }
        }

        // Log messages for grabbing state changes
        if (!wasGrabbing && isCurrentlyGrabbing)
        {
            GrabEvent.Invoke(true);
            Debug.Log("Object has been grabbed.");
        }
        else if (wasGrabbing && !isCurrentlyGrabbing)
        {
            GrabEvent.Invoke(false);
            Debug.Log("Object has been released.");
        }

        // Update the previous grabbing state and the tile's held status
        wasGrabbing = isCurrentlyGrabbing;
    }

    private void OnTriggerEnter(Collider other)
    {
        int index = System.Array.IndexOf(handPinchColliders, other);
        if (index != -1)
        {
            isColliderColliding[index] = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        int index = System.Array.IndexOf(handPinchColliders, other);
        if (index != -1)
        {
            isColliderColliding[index] = false;
        }
    }


    [System.Serializable]
    public class BoolEvent : UnityEvent<bool>{}

}
