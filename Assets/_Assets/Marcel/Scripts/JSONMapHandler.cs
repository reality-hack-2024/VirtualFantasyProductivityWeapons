using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.IO;
using System;

public class JSONMapHandler : MonoBehaviour
{
    public TileSpawner tSpawner;
    public SwordTileManager tMan;

    public LatLongToSphere LatLongToSphere;
    //public StringEvent TileEvent;
    public TextAsset jsonData; // Attach your JSON data file in the Unity Editor

    public FlightData demoFlight;
    public GameObject listeningMessage;

    void Start()
    {
        LoadDataFromJSON();
    }

    public bool fake;
    private void Update()
    {
        if (fake)
        {
            fake = false;
            AddDemo();
        }
    }

    void LoadDataFromJSON()
    {
        if (jsonData == null) return;

        // Parse the JSON data
        CombinedData data = JsonUtility.FromJson<CombinedData>(jsonData.text);

        // Add locations to the map
        foreach (var location in data.locations)
        {
            AddLocationToSword(location);

        }

        // Add flights as arcs to the map
        foreach (var flight in data.flights)
        {
            AddArcToMap(flight);
        }
    }

    /*
    void AddLocationToMap(LocationData locationData)
    {
        Debug.Log($"Adding {locationData.locationName} at lat:{locationData.coordinates.lat}, lon: {locationData.coordinates.lng}");

        var coords = new Coordinates { lat = locationData.coordinates.lat, lng = locationData.coordinates.lng };
        var location = new Location
        {
            locationName = locationData.locationName,
            coordinates = coords
        };

        LatLongToSphere.AddPin(location);
    }*/



    void AddLocationToSword(LocationData locationData)
    {
        Debug.Log($"Adding {locationData.locationName} to Sword");
        // Assuming you have a way to get the corresponding Transform for each location
        Transform tileTransform = tSpawner.SpawnTile(locationData.locationName).transform;

        // Use the track and time from location to position the tile
        tMan.PositionTileBasedOnTrackAndTime(tileTransform, locationData.track, locationData.time + 2f);

    }



    void AddArcToMap(FlightData flight)
    {
        Debug.Log($"Adding arc from {flight.origin.locationName} to {flight.destination.locationName}");

        LatLongToSphere.AddArc(flight.origin, flight.destination);

       // TileEvent.Invoke($"{flight.origin.locationName} to {flight.destination.locationName}");
    }


    public void AddDemo()
    {
        listeningMessage?.SetActive(false);
        AddArcToMap(demoFlight);

        //var newLocation = new LocationData() { locationName = };
        tSpawner.Spawn($"{demoFlight.origin.locationName} to {demoFlight.destination.locationName}");
        //AddLocationToSword(newLocation);

        
    }

    [Serializable]
    public class LocationData
    {
        public string locationName;
        public Coordinates coordinates;
        public int track;
        public float time;
    }

    [Serializable]
    public class FlightData
    {
        public Location origin;
        public Location destination;
        public int track;
        public float time;
    }

    [Serializable]
    public class LocationsWrapper
    {
        public LocationData[] locations;
    }

    [Serializable]
    public class FlightsWrapper
    {
        public FlightData[] flights;
    }

    [Serializable]
    public class CombinedData
    {
        public LocationData[] locations;
        public FlightData[] flights;
    }


}
