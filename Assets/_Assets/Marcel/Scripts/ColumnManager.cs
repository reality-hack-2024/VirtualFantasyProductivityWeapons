using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnManager : MonoBehaviour
{
    public int numberColumns;
    public float distanceBetweenCenter;
    public Column[] columns;

    public GameObject columnPrefab;
    public Transform spawnOriginTrans;
    public Column currentColumn;

    // Start is called before the first frame update
    void Start()
    {
        
        columns = new Column[numberColumns];
        Init();
    }

    // Update is called once per frame
    void Update()
    {
       // transform.tra
    }

    public void Init()
    {
        //Vector3 pos;
        for (int i = 0; i < numberColumns; i++)
        {
            var go = Instantiate(columnPrefab, spawnOriginTrans.position, spawnOriginTrans.rotation, spawnOriginTrans);
            go.transform.localPosition = Vector3.right * distanceBetweenCenter * i;
            var c = go.GetComponentInChildren<Column>();
            columns[i] = c;
            c.cMan = this;
        }
    }

    public void ChangeCurrentColumn( Column c)
    {
        if (currentColumn == c)
            return;

        if (currentColumn != null)
        {
            currentColumn.candidateTile = null;
            currentColumn.GhostHide();
        }

        currentColumn = c;
    }
}
